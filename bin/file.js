/*
 * m
 * https://github.com/raklaptudirm/m
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

const {
  readFileSync,
  writeFileSync,
  readdirSync,
  existsSync,
  lstatSync,
  mkdirSync,
} = require("fs")
const { dirname, isAbsolute, normalize } = require("path")

module.exports = {
  read: function (path) {
    return readFileSync(path).toString()
  },
  readRaw: function (path) {
    return readFileSync(path)
  },
  write: function (path, data) {
    writeFileSync(path, data)
  },
  touch: function (name, init) {
    if (!existsSync(name)) {
      mkdirSync(dirname(name), { recursive: true })
      writeFileSync(name, init)
    }
  },
  exists: function (name) {
    return existsSync(name)
  },
  getFiles: function (dir) {
    let files = []
    readdirSync(dir).forEach(file => {
      if (lstatSync(`${dir}\\${file}`).isFile()) files.push(file)
      else
        this.getFiles(`${dir}\\${file}`).forEach(item => {
          files.push(`${file}\\${item}`)
        })
    })
    return files
  },
  toAbsolute: function (path) {
    if (isAbsolute(path)) return path
    return normalize(`${process.cwd()}\\${path}`)
  },
}
