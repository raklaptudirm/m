/*
 * m
 * https://github.com/raklaptudirm/m
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

const files = require("./file.js")
const E = require("./escape.js")

class templates {
  constructor(list) {
    this.templates = list
  }
  updateList() {
    files.write(`${__dirname}/../config`, JSON.stringify(this.templates))
  }
  add(name, path) {
    for (const i of this.templates) {
      if (i.name === name) {
        console.log("Template already exists.")
        return
      }
    }
    path = files.toAbsolute(path)
    this.templates.push({ name: name, path: path })
    console.log(`Added template ${name} [ linked to ${path} ]`)
    this.updateList()
  }
  delete(name) {
    for (const i in this.templates) {
      if (this.templates[i].name === name) {
        this.templates.splice(i, 1)
        console.log(`Deleted template ${name} sucessfully.`)
        this.updateList()
        return
      }
    }
    console.log(`Template with the name ${name} not found.`)
  }
  reset(name, path) {
    path = files.toAbsolute(path)
    for (const i of this.templates) {
      if (i.name === name) {
        i.path = path
        console.log(`Updated path of ${name} to ${path}`)
        this.updateList()
        return
      }
    }
    console.log(`Template with the name ${name} not found.`)
  }
  rename(name, newName) {
    for (const i of this.templates) {
      if (i.name === name) {
        i.name = newName
        console.log(`Updated name of ${name} to ${newName}`)
        this.updateList()
        return
      }
    }
    console.log(`Template with the name ${name} not found.`)
  }
  clone(name) {
    for (const i of this.templates) {
      if (i.name === name) {
        const templateFiles = files.getFiles(i.path)
        const length = templateFiles.length
        console.log(`Cloning Template: ${E.CURSOR.SAVE}`)
        templateFiles.forEach((item, index) => {
          process.stdout.write(
            `${E.CURSOR.RESTORE}${E.ERASE.END_FROM_CURSOR}${
              E.CURSOR.RESTORE
            }[${item}] ${Math.round(((index + 1) / length) * 100)}%, (${
              index + 1
            }/${length})`
          )
          files.touch(item, files.readRaw(`${i.path}/${item}`))
        })
        console.log(", done.")
        return
      }
    }
    return
  }
  stringify(name) {
    for (const i of this.templates) {
      if (i.name === name) {
        return `${i.name} -> ${i.path}\n${files
          .getFiles(i.path)
          .map((item, index) => `  ${index + 1}. ${item}`)
          .join("\n")}`
      }
    }
    return `Template with the name ${name} not found.`
  }
  list() {
    return this.templates
      .map(item => `• ${item.name} -> ${item.path}`)
      .join("\n")
  }
}

module.exports = templates
