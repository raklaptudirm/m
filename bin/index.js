#!/usr/bin/env node

/*
 * m
 * https://github.com/raklaptudirm/m
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

/*
 * m --add <alias> <path>
 * m <alias>
 */

const files = require("./file.js")
const args = process.argv.slice(2)
const Template = require("./template.js")
const name = /[a-zA-Z0-9_:;,.<>?!@#$%^&*(){}[]]+/

;(function () {
  function assert(num) {
    if (args.length === num + 1) return
    console.log(`Expected ${num} arg(s), received ${args.length - 1}.`)
    process.exit()
  }

  if (!files.exists(`${__dirname}/../config`))
    files.touch(`${__dirname}/../config`, "[]")
  const _template = new Template(
    JSON.parse(files.read(`${__dirname}/../config`))
  )
  switch (args[0]) {
    case "-l":
    case "--list":
      assert(0)
      console.log(_template.list())
      break
    case "-a":
    case "--add":
      assert(2)
      _template.add(args[1], args[2])
      break
    case "-d":
    case "--delete":
      assert(1)
      _template.delete(args[1])
      break
    case "-g":
    case "--get":
      assert(1)
      console.log(_template.stringify(args[1]))
      break
    case "-rs":
    case "--reset":
      assert(2)
      _template.reset(args[1], args[2])
      break
    case "-rn":
    case "--rename":
      assert(2)
      _template.rename(args[1], args[2])
      break
    case "-v":
    case "--version":
      assert(0)
      console.log("v" + require("../package.json").version)
      break
    case undefined:
      console.log(
        `Usage: m <template_name>\n       m [flags] [options]\n\nFlags:\n\n  -l,  --list                    List all the tracked templates.\n  -a,  --add <name> <path>       Add a new template <name> at <path>.\n  -d,  --delete <name>           Delete the tracked template <name>.\n  -g,  --get <name>              Get information about template <name>.\n  -rs, --reset <name> <path>     Change the <name> template's path to <path>.\n  -rn, --rename <name> <new>     Change template <name>'s name to  <new>.\n  -v,  --version                 See what version of m you are using.`
      )
      break
    default:
      assert(0)
      _template.clone(args[0])
  }
})()
