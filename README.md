<h1 align="center"> <code>m</code> </h1>

<h3>
<hr>

You have a wonderful project idea.

You create a new repository.

You start coding.

```js
const fs = require("fs")
const crypto = require("crypto")

const args = process.argv.sli|
```

Oh wait! You need a `README.md` file. And a `LICENSE`. And a `.gitignore`. And a `.gitattributes`. And...

You go to an older repository and start copying the files. Except that you have too many things to copy.

"F\*\*k!", you say. "Why isn't there a better way to do this?"

Well there is. An you have found it!! 🎉🎉

<hr>
</h3>

<h1> Features </h1>

`m` is a minimalistic template manager to make all of this a piece of cake.

- Simply create templates with a single command.
- Clone templates into new and existing repos.
- Edit templates using your favourite editor.
- Rename, reset, delete templates easily.

<h1> Installation </h1>

```
$ git clone https://github.com/raklaptudirm/m.git
$ cd m
$ npm install --global
```

<h1> API </h1>

## `m --version`

Get the current installed version of `m`.

Alias: `-v`

```
$ m --version
v1.0.0
```

## `m --add <name> <path>`

Add a new template `name` from `path`. The `path` may be relative or absolute.

Alias: `-a`

```
$ m --add template .
Added template template [ linked to /current/working/directory ]
```

## `m --reset <name> <path>`

Change the reference path of template `name` to `path`.

Alias: `-rs`

```
$ m --reset template /new/path
Updated path of template to /new/path
```

## `m --rename <name> <newName>`

Change the name of template `name` to `newName`.

Alias: `-rn`

```
$ m --rename template temp
Updated name of template to temp
```

## `m --delete <name>`

Delete the template `name`.

Alias: `-d`

```
$ m --delete temp
Deleted template temp sucessfully.
```

## `m --get <name>`

Get details of template `name`.

Alias: `-g-

```
$ m --get temp
temp -> /path/to/template
  1. README.md
  2. LICENSE
  3. .gitignore
  4. .gitattributes
  5. src\index.js
```

## `m --list`

List all the existing templates.

Alias: `-l`

```
$ m --list
• template1 -> /path1
• template2 -> /path2
• template3 -> /path3
```

## `m <name>`

Clone a template `name` into the `cwd`.

```bash
$ m template
Cloning Template: [README.md] 100%, (4/4), done.
```

# Editing templates:

Editing templates is extremely simple in `m`. When you clone a template, `m` copies all the files from the referenced path to the `cwd`. So, just editing the files in the referenced path will edit the template.
